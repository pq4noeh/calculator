(function(global) {
'use strict';
  /******************************************************************************************************************************************
  *
  *                                                              LOGIC
  *
  *******************************************************************************************************************************************/
  var operands, symbols, operationMaps;
  operationMaps = {
    'plus':'+',
    'minus':'-',
    'times':'*',
    'divide':'/'

  }
  operands = (function() {
    var operands = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var decimalSeparator = [',','.'];
    var validOperands = operands.concat(decimalSeparator);
    return {
      isValidOperand: function(v) {
        return validOperands.indexOf(v) !== -1;
      },
      isDecimalSeparator: function(v){
        return decimalSeparator.indexOf(v) !== -1;
      }
    };
  })();
  symbols = (function() {
    var validSymbols, symbolsFunctions;
    validSymbols = {
      '+': 'plus',
      '-': 'minus',
      '*': 'times',
      '/': 'dividedBy'
    };
    symbolsFunctions = {
      isValidSymbol: function(s) {
        return typeof validSymbols[s] !== 'undefined';
      },
      bigNumberMethod: function(s) {
        if (!symbolsFunctions.isValidSymbol(s)) {
          throw new Error("Not a valid symbol: " + s);
        }
        return validSymbols[s];
      },
      mayPrecedeOperand: function(s) {
        return s === '-';
      }
    };
    return symbolsFunctions;
  })();

  function CalculatorElement(type, e) {
    if (type === 'operand') {
      if (!operands.isValidOperand(e)) {
        throw new Error('No valid operand: ' + e);
      }
    } else if (type === 'symbol') {
      if (!symbols.isValidSymbol(e)) {
        throw new Error('No valid symbol: ' + e);
      }
    } else {
      throw new Error('No valid CalculatorElement type: ' + type);
    }
    this.type = type;
    this.element = e;
    this.append = this.append.bind(this);
    this.isOperand = this.isOperand.bind(this);
    this.isSymbol = this.isSymbol.bind(this);
    this.toString = this.toString.bind(this);
  }
  CalculatorElement.prototype.append = function(e) {
    if (this.isSymbol()) {
      throw new Error('Append operator is not allowed to symbols element');
    } else if (!operands.isValidOperand(e)) {
      throw new Error('No valid operand: ' + e);
    }
    this.element += e;
  };
  CalculatorElement.prototype.bigNumberMethod = function() {
    if (this.isOperand()) {
      throw new Error('bigNumberMethod method is not allowed for operand elements');
    }
    return symbols.bigNumberMethod(this.element);
  };
  CalculatorElement.prototype.isOperand = function() {
    return this.type === 'operand';
  };
  CalculatorElement.prototype.isSymbol = function() {
    return this.type === 'symbol';
  };
  CalculatorElement.prototype.toString = function() {
    return this.element;
  };

  function Calculator() {
    this.elements = [];
    this.currentElement = null;
    this.addElement = this.addElement.bind(this);
    this.addNewOperand = this.addNewOperand.bind(this);
    this.addNewSymbol = this.addNewSymbol.bind(this);
    this.pushCurrent = this.pushCurrent.bind(this);
    this.canAddNewOperator = this.canAddNewOperator.bind(this);
    this.appendToOperand = this.appendToOperand.bind(this);
  }
  Calculator.prototype.addElement = function(e) {
    if (operands.isValidOperand(e)) {
      if (this.currentElement === null || this.currentElement.isSymbol()) {
        this.addNewOperand(e);
      } else {
        this.appendToOperand(e);
      }
    } else if (symbols.isValidSymbol(e)) {
      this.addNewSymbol(e);
    } else {
      throw new Error("Not a valid symbol neither operand: " + e);
    }
  };
  Calculator.prototype.appendToOperand = function(e){
    e = e === ',' ? '.' : e;
    if(operands.isDecimalSeparator(e) && this.currentElement.element.indexOf(e) !== -1){
      throw new Error("Can not add to an operand multiple times the decimal separator.");
    } else { 
      this.currentElement.append(e);
    }
  };
  Calculator.prototype.addNewOperand = function(e) {
    if(this.canAddNewOperator()){
      if (this.currentElement !== null) {
        this.elements.push(this.currentElement);
      }
      this.currentElement = new CalculatorElement('operand', e);  
    }
  };
  Calculator.prototype.addNewSymbol = function(e) {
    if (this.canAddNewSymbol(e)) {
      if (this.currentElement !== null) {
        this.elements.push(this.currentElement);
      }
      this.currentElement = new CalculatorElement('symbol', e);
    }
  };
  Calculator.prototype.canAddNewOperator = function(){
    return (this.currentElement === null && (this.elements.length === 0 || this.elements[this.elements.length-1].isSymbol())) || 
    (this.currentElement && this.currentElement.isSymbol());
  };
  Calculator.prototype.canAddNewSymbol = function(e) {
    if (this.currentElement !== null) return this.currentElement.isOperand();
    else if (this.elements.length > 0) return this.elements[this.elements.length - 1].isOperand();
    return symbols.mayPrecedeOperand(e);
  };
  Calculator.prototype.pushCurrent = function() {
    if (this.currentElement !== null) {
      this.elements.push(this.currentElement);
      this.currentElement = null;
    }
  };
  Calculator.prototype.getResult = function() {
    var results, result, c, elementsLength, methodName, parameter;
    results = [];
    this.pushCurrent();
    elementsLength = this.elements.length;
    if (elementsLength > 0) {
      result = new BigNumber(this.elements[0]);
      for (c = 1; c < elementsLength - 1; c += 2) {
        methodName = this.elements[c].bigNumberMethod();
        parameter = this.elements[c + 1].element;
        result = result[methodName](parameter);
        results.push(result.round(2).valueOf().replace('.',','));
      }
    } 
    return results;
  };
  global.Calc = Calculator;

  /******************************************************************************************************************************************
  *
  *                                                              VIEW
  *
  *******************************************************************************************************************************************/
  var calcViewHtml = '<div id="calcPanel" class="calc-panel"><a id="closeCalc" style="float:right" href="javascript:void(0)"> close</a><div class="calc-container"><div class="calc-elements-container"><ul id="calcElements" class="calc-elements"></ul></div><table class="calc"><tbody><tr><td colspan="4" id="output" class="calc-output">0</td></tr><tr><td><button type="button" id="input_7">7</button></td><td><button type="button" id="input_8">8</button></td><td><button type="button" id="input_9">9</button></td><td><button type="button" id="input_divide">/</button></td></tr><tr><td><button type="button" id="input_4">4</button></td><td><button type="button" id="input_5">5</button></td><td><button type="button" id="input_6">6</button></td><td><button type="button" id="input_times">*</button></td></tr><tr><td><button type="button" id="input_1">1</button></td><td><button type="button" id="input_2">2</button></td><td><button type="button" id="input_3">3</button></td><td><button type="button" id="input_minus">-</button></td></tr><tr><td colspan="2"><button type="button" id="input_0">0</button></td><td><button type="button" id="input_,">,</button></td><td><button type="button" id="input_plus">+</button></td></tr><tr><td colspan="3"><button type="button" class="calc-result" id="getValueBtn">= </button></td><td><button type="button" id="reset" class="calc-reset" ></button></td></tr></tbody></table></div></div>'

  function CalcView(options){
    this.drawCalcView();
    this.options = options;

    this.output = document.getElementById('output');
    this.getValueBtn = document.getElementById('getValueBtn');
    this.resetBtn = document.getElementById('reset');
    this.calcElements = document.getElementById('calcElements');
    this.calcElementsContainer = document.getElementsByClassName('calc-elements-container')[0];
    this.container = document.getElementById('calcPanel');

    this.addElement = this.addElement.bind(this);
    this.showResult = this.showResult.bind(this);
    this.bindKeyStrokes = this.bindKeyStrokes.bind(this);
    this.bindOperandsButtons = this.bindOperandsButtons.bind(this);
    this.bindGetValueButton = this.bindGetValueButton.bind(this);
    this.bindResetButton = this.bindResetButton.bind(this);
    this.init = this.init.bind(this);
    this.drawResult = this.drawResult.bind(this);
    this.drawCalcHistory = this.drawCalcHistory.bind(this);
    this.manageKeyStrokeEvent = this.manageKeyStrokeEvent.bind(this);
    this.moveElementsContainerScrollToEnd = this.moveElementsContainerScrollToEnd.bind(this);
    this.close = this.close.bind(this);

    document.getElementById('closeCalc').addEventListener('click', this.close);

    this.init();
  }
  CalcView.prototype.close = function(){
    var results = this.calc.getResult();
    this.result = (results && results.length > 0) ? results[results.length-1] : 0;
    window.removeEventListener('keypress', this.manageKeyStrokeEvent);
    document.body.removeChild(document.getElementsByClassName('calc-overlay')[0]);
    if(this.options && typeof this.options.onClose === 'function'){
      this.options.onClose(this.result);
    }
  }
  CalcView.prototype.drawCalcView = function(){
    var calcOverlay, calc;
    calcOverlay = document.getElementById('calcOverlay');
    if(calcOverlay === null){
      calcOverlay = document.createElement('div');
      calcOverlay.className = 'calc-overlay';
    } 
    calcOverlay.innerHTML = calcViewHtml;
    document.body.appendChild(calcOverlay);
  }
  CalcView.prototype.moveElementsContainerScrollToEnd = function(){
    this.calcElementsContainer.scrollTop = this.calcElementsContainer.scrollHeight;
  };
  CalcView.prototype.addElement = function(e){
    this.calc.addElement(e);
    if(this.calc.currentElement){
      setTextContent(this.output, this.calc.currentElement.toString());
    }
    if(this.calc.currentElement && this.calc.currentElement.isSymbol()){
      this.drawCalcHistory(this.calc.getResult());
    }
  };
  CalcView.prototype.showResult = function(){
    var results = this.calc.getResult();
    var lastResult = results.length === 0 ? 0 : results[results.length-1];
    setTextContent(this.output, lastResult);
    this.drawCalcHistory(results, true);
    this.drawResult(lastResult);
  };
  CalcView.prototype.manageKeyStrokeEvent = function(e){
    var oEvent, key;
    oEvent = e || window.event;
    key = oEvent.keyCode || oEvent.which;
    if(key === 13){
      this.showResult();
    }else{
      this.addElement(String.fromCharCode(key));
    }
  };
  CalcView.prototype.bindKeyStrokes = function(){
    if(this.container !== null && !this.bindKeyStrokes.binded){
      window.addEventListener('keypress', this.manageKeyStrokeEvent);
      this.bindKeyStrokes.binded = true;
    }
  };
  CalcView.prototype.bindOperandsButtons = function(){
    var elements, forEach, _this;
    if(!this.bindOperandsButtons.binded){
      _this = this;
      elements = document.querySelectorAll('[id*="input_"]');
      forEach = Array.prototype.forEach;
      forEach.call(elements, function(e) {
        var element = e.id.replace(/^input_/, '');
        element = operationMaps[element] ? operationMaps[element] : element;
        var listener = (function(element) {
            return function(){ _this.addElement(element); };
          })(element);
        e.addEventListener('click', listener);
      });
      this.bindOperandsButtons.binded = true;
    }
  };
  CalcView.prototype.bindGetValueButton = function(){
    if (this.getValueBtn !== null && !this.bindGetValueButton.binded) {
      this.getValueBtn.addEventListener('click', this.showResult);
      this.bindGetValueButton.binded = true;
    }
  };
  CalcView.prototype.bindResetButton = function() {
    if (this.resetBtn !== null && !this.bindResetButton.binded) {
      this.resetBtn.addEventListener('click', this.init);
      this.bindResetButton.binded = true;
    }
  };
  CalcView.prototype.init = function(){
    this.calc = new Calculator();
    setTextContent(this.output, '0');
    this.bindOperandsButtons();
    this.bindGetValueButton();
    this.bindKeyStrokes();
    this.bindResetButton();
    this.drawCalcHistory();
  };
  CalcView.prototype.drawResult = function (result) {
    this.calcElements.appendChild(generateResult(result));
    this.moveElementsContainerScrollToEnd();
  };
  CalcView.prototype.drawCalcHistory = function(results, omitLastResultItem) {
    var i= 0;
    results = results || [];
    if(omitLastResultItem && results.length > 0){
      results.pop();
    }
    this.calcElements.innerHTML = '';
    for(;i < this.calc.elements.length; i++){
      if(i === 0){
        this.calcElements.appendChild(generateFigure(this.calc.elements[i].toString()));
      } else {
        this.calcElements.appendChild(
          generateOperationFigure(
            this.calc.elements[i].toString(), 
            (this.calc.elements[i+1]||'').toString()
          )
        );
        if(results.length > 0){
          this.calcElements.appendChild(generatePartialResult(results.shift()));
        }
        i++;
      }
    }
    this.moveElementsContainerScrollToEnd();
  };
  function setTextContent(ctrl, text){
    ctrl.textContent = text;
  }
  function generatePartialResult(v){
    var e = generateFigure(v);
    e.childNodes[0].className = 'partial-result';
    return e;
  }

  function generateResult(v){
    var e = generateFigure(v);
    e.childNodes[0].className = 'result';
    return e;
  }
  function generateFigure(v){
    var li, span;
    li = document.createElement('li');
    span = document.createElement('span');
    setTextContent(span, v);
    li.appendChild(span);
    return li;
  }

  function generateOperationFigure(s, o){
    var e, span, deleteBtn;
    e = generateFigure(s);
    e.childNodes[0].className = 'symbol';

    span = document.createElement('span');
    setTextContent(span, o);
    span.className = 'operand';
    e.appendChild(span);
    return e;
  }

  global.CalcView = CalcView;

})(this);