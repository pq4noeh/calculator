(function() {
	"use strict";
	var expect = chai.expect;
	var sut = null;
	describe('Calc', function() {
		beforeEach(function() {
			sut = new window.Calc();
		});
		describe('input management', function() {
			it('If does not allow to input figures if last added element was an operand', function() {
				var input = [
					"33459",
					"42235"];
				input[0].split('').forEach(sut.addElement);
				sut.pushCurrent();
				expect(sut.currentElement).to.be.null;
				input[1].split('').forEach(sut.addElement);
				expect(sut.currentElement).to.be.null;
			});
			it('If you try to add more than one time , to an operand it throws an error', function() {
				sut.addElement('2');
				sut.addElement(',');
				sut.addElement('3');
				expect(function() {
					sut.addElement(',');
				}).to.
				throw ("Can not add to an operand multiple times the decimal separator.");
				expect(function() {
					sut.addElement('.');
				}).to.
				throw ("Can not add to an operand multiple times the decimal separator.");
			});
			it('If first element added is a symbol it does not add it, unles it is a minus', function() {
				var invalidSymbols = '+*/'.split('');
				invalidSymbols.forEach(function(s) {
					sut.addElement(s);
					expect(sut.currentElement).to.be.null;
				});
				sut.addElement('-');
				expect(sut.currentElement).to.not.be.null;
				expect(sut.currentElement.element).to.be.equals('-');
				expect(sut.currentElement.isSymbol()).to.be.true;
			});
			it('If we add an invalid element it throws an error.', function() {
				var addInvalidElement = function() {
					sut.addElement('z');
				}
				expect(addInvalidElement).to.throw ('Not a valid symbol neither operand: z')
			});
			it('If we add a figure it gets added.', function() {
				var newElement = '2';

				expect(sut.elements).to.be.empty;
				expect(sut.currentElement).to.be.null;

				sut.addElement(newElement);

				expect(sut.elements).to.be.empty;
				expect(sut.currentElement).to.not.be.null;
				expect(sut.currentElement.isOperand()).to.be.true;
				expect(sut.currentElement.element).to.be.equals(newElement);
				expect(sut.currentElement.toString()).to.be.equals(newElement);
			});
			it('If we add several figures one after other they get combined.', function() {
				var figureElements = '248190'.split('');
				expect(sut.currentElement).to.be.null;
				figureElements.forEach(function(e, i) {
					var expectedElement = figureElements.slice(0, i + 1).join('');
					sut.addElement(e);
					expect(sut.currentElement.isOperand()).to.be.true;
					expect(sut.currentElement.element).to.be.equals(expectedElement);
					expect(sut.currentElement.toString()).to.be.equals(expectedElement);
				});
			});
			it('If we add a symbol after a figure the figure gets promoted.', function() {
				function esPar(i) {
					return Boolean(i % 2 === 0);
				}
				var elements = [
					'7865'.split(''), ['+'],
					['3']
				];
				elements.forEach(function(e, i) {
					var expectedElement = e.join('');
					var c = i - 1;
					e.forEach(sut.addElement);
					if (esPar(i)) {
						expect(sut.currentElement.isOperand()).to.be.true;
						expect(sut.currentElement.isSymbol()).to.be.false;
					} else {
						expect(sut.currentElement.isOperand()).to.be.false;
						expect(sut.currentElement.isSymbol()).to.be.true;
					}
					expect(sut.elements).to.have.length(i);
					for (; c >= 0; c--) {
						expect(sut.elements[c].element).to.be.equals(elements[c].join(''));
						expect(sut.elements[c].toString()).to.be.equals(elements[c].join(''));
					}
					expect(sut.currentElement.element).to.be.equals(expectedElement);
					expect(sut.currentElement.toString()).to.be.equals(expectedElement);
				});
			});
		});
		it('Allows to push current element into elements', function() {
			var figureElements = '248190'.split('');
			var expectedElement = figureElements.join('');
			figureElements.forEach(sut.addElement);
			expect(sut.elements).to.be.empty;
			expect(sut.currentElement.element).to.be.equals(expectedElement);
			sut.pushCurrent();
			expect(sut.elements[0].element).to.be.equals(expectedElement);
			expect(sut.currentElement).to.be.null;
		});
		describe('Get result', function() {
			it('if they are none operation an empty array is returned.', function() {
				expect(sut.getResult()).to.be.an('array');
				expect(sut.getResult()).to.be.empty;
				sut.addElement('2');
				expect(sut.getResult()).to.be.an('array');
				expect(sut.getResult()).to.be.empty;
			});
			it('Allows get the result of an operation', function() {
				var elements = [
					['2', '5'],
					['*'],
					['4'],
					['+'],
					['8'],
					['/'],
					['2']
				];
				elements.forEach(function(e) {
					e.forEach(sut.addElement);
				});
				expect(sut.getResult()).to.have.members(['100','108','54']);
			});
			it('If the last element is a symol it omits it.', function() {
				var elements = [
					['2', '5'],
					['*'],
					['4'],
					['+'],
					['8'],
					['/'],
					['2'],
					['*']
				];
				elements.forEach(function(e) {
					e.forEach(sut.addElement);
				});
				expect(sut.getResult()).to.have.members(['100','108','54']);
			});

		});
	});
}).call(this);