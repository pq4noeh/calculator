var outputResult = null
var openCalc = function oCalc(){
  return new CalcView({
    showAsPopup:true, 
    onClose: function(result){
      outputResult = result;
    }
  });
};

module.exports = {
  'We can open the calculator, ' : function (br) {
    br
      .url('localhost:8080/calculadora.html')
      .execute(openCalc)
      .assert.elementPresent("#calcPanel")
  },
  'then do a basic calculation': function(br){
     br
      .click('#input_4')
      .click('#input_7')
      .click('#input_minus')
      .click('#input_2')
      .click('#getValueBtn')
  },
  ' and close it when ever we require': function(br){
    br
      .click('#closeCalc')
      .assert.elementNotPresent('#output')
      .end();
  }
};