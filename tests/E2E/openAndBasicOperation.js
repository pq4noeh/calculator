var openCalc = function oCalc(){
 return new CalcView({showAsPopup:true, onClose: function(result){alert(result);}});
};

module.exports = {
  'If we open the calendar' : function (browser) {
    browser
      .url('localhost:8080/calculadora.html')
      .execute(openCalc)
      .assert.elementPresent("#calcPanel")
  },
  'insert 25': function(browser){
    browser
      .click('#input_2')
      .click('#input_5')
      .assert.containsText('#output','25')
  },
  'sum 58': function(browser){
    browser
      .click('#input_plus')
      .click('#input_5')
      .click('#input_8')
      .assert.containsText('#output','58')
  },
  'rest 9': function(browser){
    browser
      .click('#input_minus')
      .click('#input_9')
      .assert.containsText('#output','9')
  },
  'it generates a partial result of 83': function(br){
    br.assert.containsText('.partial-result', '83')
  },
  'and a final result of 74': function(browser){
    browser
      .click('#getValueBtn')
      .assert.containsText('#output','74')
      .assert.containsText('.result', '74')
      .assert.containsText('.partial-result', '83')
      .end();
  }
};